package com.sevenlearn.a7learnstudents;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RetrofitApiService {

    @GET("experts/student")
    Call<List<Student>> getstudents();

    @POST("experts/student")
    Call<Student> saveStudent(@Body Student student);



}
