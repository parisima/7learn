package com.sevenlearn.a7learnstudents;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.net.sip.SipSession;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {


    private static final String TAG="MainActivity";

    Button buttonadd;
    List<Student> students;
    StudentAdapter studentAdapter;
    Student studentpari;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        studentpari=new Student(1,"parisima","etemadi","android",33);
    buttonadd=(Button)findViewById(R.id.buuttt);



          students=new ArrayList<>();
        Retrofit retrofit=new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://expertdevelopers.ir/api/v1/")
                .build();



        RetrofitApiService retrofitApiService=retrofit.create(RetrofitApiService.class);


        retrofitApiService.getstudents().enqueue(new Callback<List<Student>>() {



            @Override
            public void onResponse(Call<List<Student>> call, Response<List<Student>> response) {



                response.body();


                RecyclerView recyclerView=findViewById(R.id.rv_main_students);
                   recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this,RecyclerView.VERTICAL,false));
                   studentAdapter=new StudentAdapter(response.body());
                   recyclerView.setAdapter(studentAdapter);
              
            }

            @Override
            public void onFailure(Call<List<Student>> call, Throwable t) {


            }
        });

        buttonadd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                retrofitApiService.saveStudent(studentpari).enqueue(new Callback<Student>() {
                    @Override
                    public void onResponse(Call<Student> call, Response<Student> response) {

                        response.body();

                    }

                    @Override
                    public void onFailure(Call<Student> call, Throwable t) {

                    }
                });
            }
        });
    }



    // StringRequest request=new StringRequest(Request.Method.GET, "http://expertdevelopers.ir/api/v1/experts/student", new Response.Listener<String>() {
        //    @Override
         //   public void onResponse(String response) {


          //      Log.i(TAG,"onResponse: ");


          //      try {
            //        JSONArray jsonArray=new JSONArray(response);



             //       for(int i=0;i<jsonArray.length();i++){

             //           JSONObject studentJsonObject=jsonArray.getJSONObject(i);

                   //    Student student=new Student();
//                        student.setFirstName(studentJsonObject.getString("first_name"));
//                        student.setLastName(studentJsonObject.getString("last_name"));
//                        student.setCourse(studentJsonObject.getString("course"));
//                        student.setId(studentJsonObject.getInt("id"));
//                        student.setScore(studentJsonObject.getInt("score"));

                 //       students.add(student);

                    }


    //       Log.i(TAG,"on response"+students.size());

//                    RecyclerView recyclerView=findViewById(R.id.rv_main_students);
//                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this,RecyclerView.VERTICAL,false));
//                    recyclerView.setAdapter(new StudentAdapter(students));
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(),"rafte to ERror Responce",Toast.LENGTH_LONG).show();
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//
//            Log.i(TAG,"onErrrorResponse"+error);
//
//            }
//        });

//        RequestQueue requestQueue= Volley.newRequestQueue(this);
//
//        requestQueue.add(request);





